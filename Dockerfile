FROM ubuntu:latest

WORKDIR /app

MAINTAINER "Federico Maldonado para servidores de Municipalidad de San Salvador de Jujuy - 2022"

RUN apt-get update
RUN apt-get -y install ssh
RUN apt-get -y install mc
RUN apt-get -y install net-tools

# Reservo puerto 220 para ssh
EXPOSE 220

